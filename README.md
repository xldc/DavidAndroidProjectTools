# DavidAndroidProjectTools

Android开发常用功能大集合。以及知识点的详解代码

项目名称：【Android利器】

作者：[程序员小冰](http://blog.csdn.net/qq_21376985)

新浪微博：http://weibo.com/mcxiaobing 

先来看一下这个项目的总体功能图吧（这只是一小部分，会一直更新维护增加功能）：

![这里写图片描述](http://img.blog.csdn.net/20161031113558371)

![这里写图片描述](http://img.blog.csdn.net/20161020104006430)

![这里写图片描述](http://img.blog.csdn.net/20160918100607422)

更过功能欢迎下载项目中的APK文件进行体验

此项目是小冰工作中总结常用的功能大集合，防止以后用到的话重复造轮子，而且还有常用控件

以及难点的详解，注释非常啰嗦，纯粹是为了积累自己的知识，顺便方便自己查看。当然，说到各种

小功能，肯定少不了各种工具类与自定义View等等,我会和做项目一样，分包，并且要求和我自己做项目差不多

不过，每个人的习惯都不相同，具体分包不要纠结。希望大家能和我一样能看懂代码，学会理解，一起提高自我技能。

由于我也要上班，所以功能会慢慢地长期更新，维护此代码，所以，有bug或者好建议，欢迎提出。

当然，如果你有更好的功能，欢迎联系我。好知识一起共享。Thank You！下面是部分的功能知识点介绍：

包名架构图(目前,还在更戏中......)

![这里写图片描述](http://img.blog.csdn.net/20161031114217092)

【Android方面】里面主要是四大模块：

1,主要提供一些常用控件的知识点详解 

2,常用控件的一些封装，

3,app开发中常用功能的集成

a,二维码的扫描与生成功能 b,圆形头像的功能 

4,著名的一些开源框架的使用以及封装

【JAVA方面】java知识点：

1,设计模式：单例的恶汉式和懒汉式写法。

2,Map集合的存取数据详解。


里面包含工具类：

1,正则提供验证邮箱、手机号、电话号码、身份证号码、数字等方法

2,log打印日志的工具类

3，Toast封装工具类

4，Activity的管理工具类

5，通过url获取Json数据 // 通过url获取网络Bitmap数据 （httpClient所完成）

6,网络状态的判断（是否有网络，以及网络是3G,WIFI等状况）

7,多种loading等待框的介绍。

里面所使用第三方库以及（jar文件上传）：

Gson, Xutils2.6, Picasso,Universional_Imageloader, glide,OkHttp,fresco,shareSDK分享等.

项目为小冰所写，仅供参考，请勿用于任何商业。转载请说明出处。

可以直接放在AndroidStudio开发工具中运行,

记得将Constants类中的百度API秘钥改为自己的即可。

欢迎star,fork，提出更好的建议。
